 function [ cword ] = Hamming_encoder( halfbyte )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%Generator Matrix
G = [1 0 0 0 1 1 1;...
     0 1 0 0 1 0 1;...
     0 0 1 0 1 1 0;...
     0 0 0 1 0 1 1];
 
%Parity Check Matrix
 H = [1 1 1 0 1 0 0;
      1 0 1 1 0 1 0;
      1 1 0 1 0 0 1];
  
cword = mod(halfbyte*G,2);
end


  
  
