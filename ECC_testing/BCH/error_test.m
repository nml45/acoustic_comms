

halfbyte = randi(2,1,4)-1;
cword = Hamming_encoder(halfbyte);

flip_idx = randi(7)
cword(flip_idx) = ~cword(flip_idx);
halfbyte_out = Hamming_decoder(cword);

assert(isequal(halfbyte,halfbyte_out))