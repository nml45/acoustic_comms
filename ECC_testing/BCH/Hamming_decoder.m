 function [ halfbyte ] = Hamming_decoder( rword )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
    X = [0 0 0 0;
         0 0 0 1;
         0 0 1 0;
         0 0 1 1;
         0 1 0 0;
         0 1 0 1;
         0 1 1 0;
         0 1 1 1;
         1 0 0 0;
         1 0 0 1;
         1 0 1 0;
         1 0 1 1;
         1 1 0 0;
         1 1 0 1;
         1 1 1 0;
         1 1 1 1];
     
    
    %Generator Matrix
    G = [1 0 0 0 1 1 1;...
         0 1 0 0 1 0 1;...
         0 0 1 0 1 1 0;...
         0 0 0 1 0 1 1];

    %Parity Check Matrix
     H = [1 1 1 0 1 0 0;
          1 0 1 1 0 1 0;
          1 1 0 1 0 0 1];
    D = mod(X*G,2);
    [nrows,ncols] = size(H);
    parity_bits = mod(H*rword',2)
    berr_idx = -1;
    for nidx = 1:ncols
        if isequal(H(:,nidx),parity_bits)
            berr_idx = nidx
        end
    end
    cword = rword;

    if berr_idx ~= -1
       cword(berr_idx) = xor(cword(berr_idx),1);
    end

    halfbyte = cword(1:4);
end


  
  
